package com.pkglobal.user.service;

import java.util.List;
import com.pkglobal.user.entity.UserEntity;

public interface UserEntityService {

	public UserEntity getEntityById(int id);
	public List<UserEntity> getAllEntities();
	public UserEntity addEntity(UserEntity entity);
	public boolean updateEntity(UserEntity entity);
	public boolean deleteEntityById(int id);
}