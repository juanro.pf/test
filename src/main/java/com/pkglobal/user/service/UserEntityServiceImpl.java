package com.pkglobal.user.service;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pkglobal.user.entity.UserEntity;
import com.pkglobal.user.repository.UserEntityRepository;

@Service
@Transactional
public class UserEntityServiceImpl implements UserEntityService {

    private UserEntityRepository repository;

    public UserEntityServiceImpl() {

    }

    @Autowired
    public UserEntityServiceImpl(UserEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserEntity getEntityById(int id) {
        return this.repository.findById(id).get();
    }

    @Override
    public List < UserEntity > getAllEntities() {
        List < UserEntity > list = new ArrayList < > ();
        repository.findAll().forEach(e -> list.add(e));
        return list;
    }

    @Override
    public UserEntity addEntity(UserEntity entity) {
        try {
            return this.repository.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public boolean updateEntity(UserEntity entity) {
        try {
            this.repository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteEntityById(int id) {
        try {
            this.repository.deleteById(id);
            return true;
        } catch (Exception e) {
        	System.out.println("Exception in deleteById ************");
        	e.printStackTrace();
            return false;
        }

    }

}