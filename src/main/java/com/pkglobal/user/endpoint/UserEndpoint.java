package com.pkglobal.user.endpoint;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.server.endpoint.annotation.SoapHeader;

import com.pkglobal.user.entity.UserEntity;
import com.pkglobal.user.service.UserEntityService;
import com.pkglobal.user.ws.TGetUserByIdResponse;
import com.pkglobal.user.ws.TGetUserByIdRequest;
import com.pkglobal.user.ws.UserType;
import com.pkglobal.user.ws.ObjectFactory;
import com.pkglobal.user.ws.RequestHeaders;
import com.pkglobal.user.ws.ServiceStatus;
import com.pkglobal.user.ws.TAddUserRequest;
import com.pkglobal.user.ws.TAddUserResponse;
import com.pkglobal.user.ws.TGetAllUsersRequest;
import com.pkglobal.user.ws.TGetAllUsersResponse;
import com.pkglobal.user.ws.TUpdateUserRequest;
import com.pkglobal.user.ws.TUpdateUserResponse;
import com.pkglobal.user.ws.TDeleteUserRequest;
import com.pkglobal.user.ws.TDeleteUserResponse;

@Endpoint
public class UserEndpoint {

	public static final String NAMESPACE_URI = "http://pkglobal.com/user/ws";

	private UserEntityService service;

	public UserEndpoint() {

	}

	@Autowired
	public UserEndpoint(UserEntityService service) {
		this.service = service;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllUsersRequest")
	@ResponsePayload
	public JAXBElement<TGetAllUsersResponse> getAllUsers(@RequestPayload TGetAllUsersRequest request, @SoapHeader(
	          value = "{http://pkglobal.com/user/ws}requestHeaders") SoapHeaderElement soapHeaderElement) {
		String tocken = "unknown";
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			JAXBElement<RequestHeaders> headers =
					(JAXBElement<RequestHeaders>) unmarshaller
					.unmarshal(soapHeaderElement.getSource());
			
			RequestHeaders requestSoapHeaders = headers.getValue();
			tocken = requestSoapHeaders.getTocken();
		} catch (Exception e) {
			System.out.println(tocken);
		}
		if("tocken-123".equals(tocken)) {
			TGetAllUsersResponse response = new TGetAllUsersResponse();
			serviceStatus.setStatusCode("SUCCESS");
			serviceStatus.setMessage("Valid authorization tocken");
			response.setServiceStatus(serviceStatus);
			
			List<UserType> userTypeList = new ArrayList<UserType>();
			List<UserEntity> userEntityList = service.getAllEntities();
			for (UserEntity entity : userEntityList) {
				UserType userType = new UserType();
				BeanUtils.copyProperties(entity, userType);
				userTypeList.add(userType);
			}
			response.getUserType().addAll(userTypeList);
			
			QName qname = new QName("getUserByIdRequest");
			JAXBElement<TGetAllUsersResponse> jResponse = new JAXBElement<TGetAllUsersResponse>(qname, TGetAllUsersResponse.class, response);
			return jResponse;			
		} else {
			TGetAllUsersResponse response = new TGetAllUsersResponse();
			serviceStatus.setStatusCode("FAILED");
			serviceStatus.setMessage("Invalid authorization tocken");
			response.setServiceStatus(serviceStatus);
			QName qname = new QName("getUserByIdRequest");
			JAXBElement<TGetAllUsersResponse> jResponse2 = new JAXBElement<TGetAllUsersResponse>(qname, TGetAllUsersResponse.class, response);
			return jResponse2;
		}

	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserByIdRequest")
	@ResponsePayload
	public JAXBElement<TGetUserByIdResponse> getUserById(@RequestPayload TGetUserByIdRequest request, @SoapHeader(
	          value = "{http://pkglobal.com/user/ws}requestHeaders") SoapHeaderElement soapHeaderElement) {
		String tocken = "unknown";
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			JAXBElement<RequestHeaders> headers =
					(JAXBElement<RequestHeaders>) unmarshaller
					.unmarshal(soapHeaderElement.getSource());
			
			RequestHeaders requestSoapHeaders = headers.getValue();
			tocken = requestSoapHeaders.getTocken();
		} catch (Exception e) {
			System.out.println(tocken);
		}
		if("tocken-123".equals(tocken)) {
			TGetUserByIdResponse response = new TGetUserByIdResponse();
			serviceStatus.setStatusCode("SUCCESS");
			serviceStatus.setMessage("Valid authorization tocken");
			response.setServiceStatus(serviceStatus);
			UserEntity userEntity = service.getEntityById(request.getId());
			UserType userType = new UserType();
			BeanUtils.copyProperties(userEntity, userType);
			response.setUserType(userType);
			QName qname = new QName("getUserByIdRequest");
			JAXBElement<TGetUserByIdResponse> jResponse = new JAXBElement<TGetUserByIdResponse>(qname, TGetUserByIdResponse.class, response);
			return jResponse;			
		} else {
			TGetUserByIdResponse response = new TGetUserByIdResponse();
			serviceStatus.setStatusCode("FAILED");
			serviceStatus.setMessage("Invalid authorization tocken");
			response.setServiceStatus(serviceStatus);
			QName qname = new QName("getUserByIdRequest");
			JAXBElement<TGetUserByIdResponse> jResponse2 = new JAXBElement<TGetUserByIdResponse>(qname, TGetUserByIdResponse.class, response);
			return jResponse2;
		}

	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addUserRequest")
	@ResponsePayload
	public JAXBElement<TAddUserResponse> addUser(@RequestPayload TAddUserRequest request, @SoapHeader(
	          value = "{http://pkglobal.com/user/ws}requestHeaders") SoapHeaderElement soapHeaderElement) {
		String tocken = "unknown";
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			JAXBElement<RequestHeaders> headers =
					(JAXBElement<RequestHeaders>) unmarshaller
					.unmarshal(soapHeaderElement.getSource());
			
			RequestHeaders requestSoapHeaders = headers.getValue();
			tocken = requestSoapHeaders.getTocken();
		} catch (Exception e) {
			System.out.println(tocken);
		}
		if("tocken-123".equals(tocken)) {
			TAddUserResponse response = new TAddUserResponse();
			serviceStatus.setStatusCode("SUCCESS");
			serviceStatus.setMessage("Valid authorization tocken");
			response.setServiceStatus(serviceStatus);
			
			UserType newUserType = new UserType();
			UserEntity newUserEntity = new UserEntity();
			newUserEntity.setUserName(request.getUserName());
			newUserEntity.setFirstName(request.getFirstName());
			newUserEntity.setLastName(request.getLastName());
			newUserEntity.setPassword(request.getPassword());
			newUserEntity.setEmail(request.getEmail());
			newUserEntity.setBirth(request.getBirth());
			newUserEntity.setRfc(request.getRfc());
			newUserEntity.setCurp(request.getCurp());
			UserEntity savedUserEntity = service.addEntity(newUserEntity);
			
			BeanUtils.copyProperties(savedUserEntity, newUserType);
			response.setUserType(newUserType);
			QName qname = new QName("addUserRequest");
			JAXBElement<TAddUserResponse> jResponse = new JAXBElement<TAddUserResponse>(qname, TAddUserResponse.class, response);
			return jResponse;			
		} else {
			TAddUserResponse response = new TAddUserResponse();
			serviceStatus.setStatusCode("FAILED");
			serviceStatus.setMessage("Invalid authorization tocken");
			response.setServiceStatus(serviceStatus);
			QName qname = new QName("addUserRequest");
			JAXBElement<TAddUserResponse> jResponse2 = new JAXBElement<TAddUserResponse>(qname, TAddUserResponse.class, response);
			return jResponse2;
		}
		
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateUserRequest")
	@ResponsePayload
	public JAXBElement<TUpdateUserResponse> updateUser(@RequestPayload TUpdateUserRequest request, @SoapHeader(
	          value = "{http://pkglobal.com/user/ws}requestHeaders") SoapHeaderElement soapHeaderElement) {
		String tocken = "unknown";
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			JAXBElement<RequestHeaders> headers =
					(JAXBElement<RequestHeaders>) unmarshaller
					.unmarshal(soapHeaderElement.getSource());
			
			RequestHeaders requestSoapHeaders = headers.getValue();
			tocken = requestSoapHeaders.getTocken();
		} catch (Exception e) {
			System.out.println(tocken);
		}
		if("tocken-123".equals(tocken)) {
			TUpdateUserResponse response = new TUpdateUserResponse();
			serviceStatus.setStatusCode("SUCCESS");
			serviceStatus.setMessage("Valid authorization tocken");
			response.setServiceStatus(serviceStatus);
			
			UserType newUserType = new UserType();
			UserEntity newUserEntity = new UserEntity();
			newUserEntity.setId(request.getId());
			newUserEntity.setUserName(request.getUserName());
			newUserEntity.setFirstName(request.getFirstName());
			newUserEntity.setLastName(request.getLastName());
			newUserEntity.setPassword(request.getPassword());
			newUserEntity.setEmail(request.getEmail());
			newUserEntity.setBirth(request.getBirth());
			newUserEntity.setRfc(request.getRfc());
			newUserEntity.setCurp(request.getCurp());
			boolean flag = service.updateEntity(newUserEntity);
			
			BeanUtils.copyProperties(newUserEntity, newUserType);
			response.setUserType(newUserType);
			QName qname = new QName("updateUserRequest");
			JAXBElement<TUpdateUserResponse> jResponse = new JAXBElement<TUpdateUserResponse>(qname, TUpdateUserResponse.class, response);
			return jResponse;			
		} else {
			TUpdateUserResponse response = new TUpdateUserResponse();
			serviceStatus.setStatusCode("FAILED");
			serviceStatus.setMessage("Invalid authorization tocken");
			response.setServiceStatus(serviceStatus);
			QName qname = new QName("updateUserRequest");
			JAXBElement<TUpdateUserResponse> jResponse2 = new JAXBElement<TUpdateUserResponse>(qname, TUpdateUserResponse.class, response);
			return jResponse2;
		}
		
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteUserRequest")
	@ResponsePayload
	public JAXBElement<TDeleteUserResponse> deleteUser(@RequestPayload TDeleteUserRequest request, @SoapHeader(
	          value = "{http://pkglobal.com/user/ws}requestHeaders") SoapHeaderElement soapHeaderElement) {
		String tocken = "unknown";
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			JAXBElement<RequestHeaders> headers =
					(JAXBElement<RequestHeaders>) unmarshaller
					.unmarshal(soapHeaderElement.getSource());
			
			RequestHeaders requestSoapHeaders = headers.getValue();
			tocken = requestSoapHeaders.getTocken();
		} catch (Exception e) {
			System.out.println(tocken);
		}
		if("tocken-123".equals(tocken)) {
			TDeleteUserResponse response = new TDeleteUserResponse();
			serviceStatus.setStatusCode("SUCCESS");
			serviceStatus.setMessage("Valid authorization tocken");
			response.setServiceStatus(serviceStatus);
			
			boolean flag = service.deleteEntityById(request.getId());
			
			response.setMessage("User with id: " + request.getId() + " deleted successfully");;
			QName qname = new QName("deleteUserRequest");
			JAXBElement<TDeleteUserResponse> jResponse = new JAXBElement<TDeleteUserResponse>(qname, TDeleteUserResponse.class, response);
			return jResponse;			
		} else {
			TDeleteUserResponse response = new TDeleteUserResponse();
			serviceStatus.setStatusCode("FAILED");
			serviceStatus.setMessage("Invalid authorization tocken");
			response.setServiceStatus(serviceStatus);
			QName qname = new QName("deleteUserRequest");
			JAXBElement<TDeleteUserResponse> jResponse2 = new JAXBElement<TDeleteUserResponse>(qname, TDeleteUserResponse.class, response);
			return jResponse2;
		}

	}
	
}