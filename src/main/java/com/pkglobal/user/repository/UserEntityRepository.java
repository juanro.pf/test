package com.pkglobal.user.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pkglobal.user.entity.UserEntity;

@Repository
public interface UserEntityRepository extends CrudRepository<UserEntity, Integer> {

}