package com.pkglobal.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSoapWebServicesUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSoapWebServicesUserApplication.class, args);
	}

}
